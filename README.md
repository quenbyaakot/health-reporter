# CastleWars health-reporter
CastleWars component. Runs on target machine and maintains a connection with [health-monitor](https://gitlab.com/quenbyaakot/health-monitor).

See also: [main repo](https://gitlab.com/quenbyaakot/main-server)
## Target Machine Setup
### Prerequisites
* [rustup](https://www.rust-lang.org/tools/install)
* systemd
### Installation
1. Clone or download this repository
2. Run `install.sh` with sudo.

Note: If you have a non-standard case (e.g. your target machine uses an init system other than systemd or you wish to compile elsewhere), you will have to modify the script to suit your needs

## Usage
`cargo r <main_host>:<port>`
