use std::{env, net::SocketAddr};
use tokio::{
    codec::{BytesCodec, Decoder},
    net::TcpStream,
    prelude::*,
};

const TRIES_COUNT: i8 = 3;

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        eprintln!("error: missing host argument");
        return;
    }

    let host: SocketAddr = args[1].parse().expect("error: invalid host argument");

    for i in 0..TRIES_COUNT {
        let stream = TcpStream::connect(host)
            .await
            .expect("error: cannot connect to host");

        let mut framed = BytesCodec::new().framed(stream);

        while let Some(message) = framed.next().await {
            match message {
                Ok(_) => eprintln!("error: recieved some unexpected bytes"),
                Err(_) => {
                    eprint!("error: connection closed. {} attempt to reconnect.", i + 1);
                    break;
                }
            }
        }
    }
}
